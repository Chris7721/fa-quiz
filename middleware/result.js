export default async function ({ store }) {
  // if the length of the answered questions isn't sames as the questions length
  if (store.state.userAnswers.length !== store.state.questions.length) {
    window.location = '/'
  }
}
