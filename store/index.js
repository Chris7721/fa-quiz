import questions from '~/static/js/questions'
export const state = () => ({
  questions,
  userAnswers: [],
})

export const mutations = {
  updateUserAnswers(state, answer) {
    const answerExists = state.userAnswers.some(
      (el) => el.question_id == answer.question_id
    )
    // if it hasn't been answered before add it to the array, else filter out the one that was there before and add the new one
    if (!answerExists) {
      state.userAnswers = [...state.userAnswers, answer]
      return
    }
    state.userAnswers = [
      ...state.userAnswers.filter(
        (userAnswer) => userAnswer.question_id !== answer.question_id
      ),
      answer,
    ]
  },
  // return store to the initial state
  resetApp(state) {
    state.userAnswers = []
  },
}

export const getters = {
  getQuestions({ questions }) {
    return questions
  },
  getUserAnswers({ userAnswers }) {
    return userAnswers
  },
  getUserResult({ userAnswers, questions }) {
    // run through all questions and check if the user's selected answeres are the actual answers
    const correctAnswers = questions.filter((question) => {
      const correctAnswer = question.options.find((option) => option.answer)
      const foundAnswer = userAnswers.find(
        (answer) => answer.option_id == correctAnswer.id
      )
      return Boolean(foundAnswer)
    })
    return (correctAnswers.length / questions.length) * 100
  },
}
